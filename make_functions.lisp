(defun make-sum (arg1 arg2)
 (list '+ arg1 arg2)
)

(defun make-product (arg1 arg2)
 (list '* arg1 arg2)
)

(defun make-power (arg1 arg2)
 (list '^ arg1 arg2)
)

(defun make-sin (arg)
 (list 'sin arg)
)

(defun make-cos (arg)
 (list 'cos arg)
)

(defun make-quotient (arg1 arg2)
 (list '/ arg1 arg2)
)

(defun make-difference (arg1 arg2)
 (list '- arg1 arg2)
)
