(defun constant? (arg)
 (numberp arg)
)

(defun variable? (arg)
 (symbolp arg)
)

(defun power? (arg)
 (if (not (atom arg))
     (equal (first arg) '^)
     nil
 )
)

(defun same-variable? (v1 v2)
 (and (variable? v1)
      (variable? v2)
      (equal v1 v2)
 )
)

(defun same-constant? (c1 c2)
 (and (constant? c1)
      (constant? c2)
      (= c1 c2)
 )
)

(defun sum? (arg)
 (if (not (atom arg))
     (equal (first arg) '+)
     nil
 )
)

(defun product? (arg)
 (if (not (atom arg))
     (equal (first arg) '*)
     nil
 )
)

(defun sin? (arg)
 (if (not (atom arg))
  (equal (first arg) 'sin)
  nil
 )
)

(defun cos? (arg)
 (if (not (atom arg))
  (equal (first arg) 'cos)
  nil
 )
)

(defun quotient? (arg)
 (if (not (atom arg))
  (equal (first arg) '/)
  nil
 )
)

(defun difference? (arg)
 (if (not (atom arg))
  (equal (first arg) '-)
  nil
 )
)
